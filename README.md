# ESTDesigner
-------------

> 关于我，欢迎关注  
  博客：http://lisonghua2010.iteye.com/

### 最新项目推荐
- https://gitee.com/sngh_lee/fabric2-manager.git

基于Draw2d Touch实现的Activiti工作流Web设计器，本设计器完全使用JQuery语法开发，集成Easyui前端框架，支持目前多种主流浏览器。此版本仅供学习，如需商业使用请联系我们，谢谢合作！

#### 示例:  
![流程绘制](https://images.gitee.com/uploads/images/2018/0912/100713_d2eb3ea6_449577.jpeg "绘制流程.JPG")
![xml](https://images.gitee.com/uploads/images/2018/0918/095559_b8bd2a77_449577.jpeg "xml文档.JPG")

### 特性

- 多浏览器支持
- 支持Activiti5以上语法结构
- 采用目前比较流行的JS库-JQuery
- 符合BPMN2.0标准
- 可视化定制
- 代码简洁易维护

### 依赖
- Draw2D V6.1.66
- Jquery V1.12.0
- EasyUI V1.4.5


### 安装部署

直接导入Eclipse，用HttpReview运行

访问地址
http://localhost:8080/estd/index.html


### 功能

- 支持拖拽式流程绘制
- 依赖流程图动态生成XML文档
- 根据XML文档生成流程图
- 支持子流程
- XML语法支持高亮显示

### Web版流程定制器预览版

![输入图片说明](https://images.gitee.com/uploads/images/2019/0513/203749_e80b4ac0_449577.jpeg "web-bpm.jpg")
 
- Web版流程定制器目前没有开源计划，可以接受商业合作

访问地址
http://47.95.253.227:8090
用户名：bpm
密码：123456

### VUE版本项目地址

https://gitee.com/tianya_studio/ESTDesigner-VUE.git

![展示1](https://images.gitee.com/uploads/images/2019/0721/120632_92ccd46f_449577.jpeg "在这里输入图片标题")
![展示2](https://images.gitee.com/uploads/images/2019/0721/120700_49ba4c0d_449577.jpeg "在这里输入图片标题")
![展示3](https://images.gitee.com/uploads/images/2019/0721/120719_e9f4360d_449577.jpeg "在这里输入图片标题")


### 联系我们
- 公司：大连逻辑向量科技有限公司
- QQ/微信：1581361
- 技术交流群：579106748
- email：luojixiangliang@sina.com
- 公众号：
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0419/144020_1af97a83_449577.jpeg "公众号.jpg")
- 官网：
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0419/144208_c95100ff_449577.jpeg "gh_3c777600693c_344.jpg")


